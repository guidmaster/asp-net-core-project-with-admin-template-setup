﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Asp_net_Core_Project_with_Admin_Template_Setup.Controllers
{
    public class ClinicController : Controller
    {
        private readonly IRepository repository;

        public ClinicController(IRepository repository)
        {
            this.repository = repository;
        }


        public IActionResult Index()
        {
            //Use a repository (that calls the API)
            //Create the model that needs to be sent to the View
            var model = repository.GetClinics();
            var clinics = new List<Clinic>();
            clinics.Add(new Clinic() { Email = "mjo@test.dk", Name = "Morten Test", Id = "12", Active=true });
            clinics.Add(new Clinic() { Email = "geg@test.dk", Name = "Gitte Test", Id = "15", Active=false});

            //Send the model to the view
            return View(clinics);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateClinic(Clinic clinic)
        {
            repository.SaveClinic(clinic);
            return RedirectToAction("Index", "Clinic");
        }

        public ActionResult Update(int id)
        {
            return View(new Clinic() { Email = "geg@test.dk", Name = "Gitte Tester", Id = "15", Active = false });
        }

        [HttpPost]
        public IActionResult UpdateClinic(Clinic clinic)
        {
            return RedirectToAction("Index", "Clinic");
        }
    }

    public interface IRepository
    {
        object GetClinics();
        void SaveClinic(Clinic clinic);
    }

    public class Clinic
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Email { get; set; }
    }
}